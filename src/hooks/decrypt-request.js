// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const CryptoJS = require('crypto-js');
const REQUEST_KEY = 'M|)|)S\/G|\/|_R3QU3ST';
const ENCRYPT_KEY = 'M|)|)S\/G|\/|';

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return function (hook) {
    // Hooks can either return nothing or a promise
    // that resolves with the `hook` object for asynchronous operations
    let token = hook.params.query.t;
    if (token) {
      // Convert url string to base64 string
      let base64Token = token.replace(/\,/g, '=').replace(/\-/g, '+').replace(/\_/g, '\/');
      // Decrypt token to origin value
      let decryptedData = CryptoJS.AES.decrypt(base64Token, REQUEST_KEY);
      // Get raw data
      let rawData = decryptedData.toString(CryptoJS.enc.Utf8);
      if (rawData) {
        let params = JSON.parse(rawData);
        //TODO Check for time valid
        if (params.hasOwnProperty('q')) {
          hook.params.query = params.q;
          return Promise.resolve(hook);
        } else {
          throw new Error('Not a valid param.');
        }
      } else {
        throw new Error('Invalid token.');
      }
    } else {
      throw new Error('No token param.');
    }
  };
};
