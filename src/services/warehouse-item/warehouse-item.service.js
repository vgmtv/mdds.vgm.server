// Initializes the `warehouse-item` service on path `/warehouse-item`
const createService = require('feathers-nedb');
const createModel = require('../../models/warehouse-item.model');
const hooks = require('./warehouse-item.hooks');
const filters = require('./warehouse-item.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'warehouse-item',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/warehouse-item', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('warehouse-item');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
