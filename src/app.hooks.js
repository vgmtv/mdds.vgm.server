// Application hooks that run for every service
const logger = require('./hooks/logger');

const encryptResponse = require('./hooks/encrypt-response');

const decryptRequest = require('./hooks/decrypt-request');

module.exports = {
  before: {
    all: [],
    find: [decryptRequest()],
    get: [decryptRequest()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [ logger() ],
    find: [encryptResponse()],
    get: [encryptResponse()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [ logger() ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
