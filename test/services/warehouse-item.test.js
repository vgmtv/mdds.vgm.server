const assert = require('assert');
const app = require('../../src/app');

describe('\'warehouse-item\' service', () => {
  it('registered the service', () => {
    const service = app.service('warehouse-item');

    assert.ok(service, 'Registered the service');
  });
});
