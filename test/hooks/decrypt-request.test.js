const assert = require('assert');
const decryptRequest = require('../../src/hooks/decrypt-request');

describe('\'decrypt-request\' hook', () => {
  it('runs the hook', () => {
    // A mock hook object
    const mock = {
      params: {
        query: {
          t: 'U2FsdGVkX19Oi5-8z9GiPLe3YZ9k7bxHzc-hKTsgLfov5lnMjtBaJNotMw3tMCJ1U1FJuoy44cHS66UOqs7AmA,,',
        },
      },
    };
    // Initialize our hook with no options
    const hook = decryptRequest();

    // Run the hook function (which returns a promise)
    // and compare the resulting hook object
    return hook(mock).then(result => {
      assert.equal(result, mock, 'Returns the expected hook object');
    });
  });
});
