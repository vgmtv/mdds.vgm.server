const assert = require('assert');
const encryptResponse = require('../../src/hooks/encrypt-response');
const CryptoJS = require('crypto-js');

describe('\'encrypt-response\' hook', () => {
  it('runs the hook', () => {
    // A mock hook object
    const mock = {
      data: {
        t: 1494991701,
        d: {
          username: 'nampdn96',
          password: 'Nazareth2@'
        },
      },
    };
    // Initialize our hook with no options
    const hook = encryptResponse();

    // Run the hook function (which returns a promise)
    // and compare the resulting hook object
    return hook(mock).then(result => {
      assert.equal(result, mock, 'Returns the expected hook object');
    });
  });
});
