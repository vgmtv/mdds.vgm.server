const CryptoJS = require('crypto-js');

const REQUEST_KEY = 'M|)|)S\/G|\/|_R3QU3ST';
const RESPONSE_KEY = 'M|)|)S\/G|\/|_R3SP0NS3';

const DEMO_REQUEST_DATA = {
    t: 1494993888,
    q: {
        TopicId: '544d9107-d57f-4aae-97a1-facef21270b1'
    }
};

var encrypted_request = CryptoJS.AES.encrypt(JSON.stringify(DEMO_REQUEST_DATA), REQUEST_KEY);
var encrypted_base64 = encrypted_request.toString();
encrypted_base64 = encrypted_base64.replace(/\=/g, ',').replace(/\+/g, '-').replace(/\//g, '_');
console.log(encrypted_base64);